package satorg.dom

import org.scalajs.dom

import scala.scalajs.js
import scala.scalajs.js.annotation.JSGlobal

@js.native
@JSGlobal
class URLSearchParams(init: String = js.native) extends js.Object {

  def has(name: js.Any): Boolean = js.native

  def getAll(name: js.Any): js.Array[String] = js.native

  def set(name: js.Any, value: js.Any): Unit = js.native

  def append(name: js.Any, value: js.Any): Unit = js.native

  override def toString: String = js.native
}

@js.native
@JSGlobal
class URL(url: String, base: String = js.native) extends dom.experimental.URL(url, base) {

  def searchParams: URLSearchParams = js.native
}
