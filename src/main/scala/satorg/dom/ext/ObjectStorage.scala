package satorg.dom.ext

import org.scalajs.dom

import scala.scalajs.js

final class ObjectStorage[A <: js.Any] private(underlying: TypedStorage[A], domStorageKey: String, default: => A) {

  def this(domExtStorage: dom.ext.Storage, domStorageKey: String, default: => A) =
    this(new TypedStorage[A](domExtStorage), domStorageKey, default)

  def get(): A = underlying(domStorageKey).getOrElse(default)

  def set(item: A): Unit = underlying(domStorageKey) = item
}
