package satorg.dom.ext

import org.scalajs.dom

import scala.scalajs.js

class TypedStorage[A <: js.Any](domExtStorage: dom.ext.Storage) {

  def length: Int = domExtStorage.length

  def apply(key: String): Option[A] = domExtStorage(key).map { js.JSON.parse(_).asInstanceOf[A] }

  def update(key: String, entry: A): Unit = { domExtStorage(key) = js.JSON.stringify(entry) }

  def clear(): Unit = domExtStorage.clear()

  def remove(key: String): Unit = domExtStorage.remove(key)

  def key(index: Int): Option[String] = domExtStorage.key(index)
}
