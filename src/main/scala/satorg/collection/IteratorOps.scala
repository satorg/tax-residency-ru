package satorg.collection

import scala.annotation.tailrec

final class IteratorOps[A](private val self: Iterator[A]) extends AnyVal {

  def groupAdjacentMap[K, B](key: A => K)(mapper: A => B): Iterator[(K, Iterator[B])] = {

    @tailrec
    def helper(it: Iterator[A], acc: Iterator[(K, Iterator[B])]): Iterator[(K, Iterator[B])] = {

      if (!it.hasNext) return acc

      val head = it.next()
      val k = key(head)
      val (l, r) = it.span { key(_) == k }

      val res = Iterator.single(k -> (Iterator.single(head) ++ l).map(mapper))
      helper(r, acc ++ res)
    }

    helper(self, Iterator.empty)
  }

  def groupAdjacentBy[K](key: A => K): Iterator[(K, Iterator[A])] = {

    groupAdjacentMap(key)(identity)
  }
}
