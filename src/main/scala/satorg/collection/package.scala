package satorg

import scala.language.{higherKinds, implicitConversions}

package object collection {

  implicit def iteratorToIteratorOps[A](it: Iterator[A]): IteratorOps[A] = new IteratorOps[A](it)

  implicit def seqToSeqOps[A, C[x] <: Seq[x]](seq: C[A]): SeqOps[A, C] = new SeqOps[A, C](seq)
}
