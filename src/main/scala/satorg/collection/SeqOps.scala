package satorg.collection

import scala.collection.generic.CanBuildFrom
import scala.language.higherKinds

final class SeqOps[A, C[x] <: Seq[x]](private val self: C[A]) extends AnyVal {

  def groupAdjacentMap[K, B](key: A => K)(mapper: A => B)
                            (implicit cb: CanBuildFrom[Nothing, B, C[B]])
  : Seq[(K, C[B])] = {

    self.
      iterator.
      groupAdjacentMap(key)(mapper).
      map { case (k, it) => k -> it.to[C] }.
      to[Seq]
  }

  def groupAdjacentBy[K](key: A => K)
                        (implicit cb: CanBuildFrom[Nothing, A, C[A]])
  : Seq[(K, C[A])] = {

    groupAdjacentMap(key)(identity)
  }
}
