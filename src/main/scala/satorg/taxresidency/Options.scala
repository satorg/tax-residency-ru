package satorg.taxresidency

import scala.scalajs.js

final class Options(val departureDateAbroad: Boolean,
                    val arrivalDateAbroad: Boolean,
                    val skipPastDates: Boolean,
                    val completeResidence: Boolean,
                    val completeMonth: Boolean)
  extends js.Object

object Options {

  lazy val Default =
    new Options(
      departureDateAbroad = true,
      arrivalDateAbroad = false,
      skipPastDates = false,
      completeResidence = false,
      completeMonth = false)

  implicit final class Ops(private val self: Options) extends AnyVal {

    def copy(departureDateAbroad: Boolean = self.departureDateAbroad,
             arrivalDateAbroad: Boolean = self.arrivalDateAbroad,
             skipPastDates: Boolean = self.skipPastDates,
             completeResidence: Boolean = self.completeResidence,
             completeMonth: Boolean = self.completeMonth) =
      new Options(
        departureDateAbroad = departureDateAbroad,
        arrivalDateAbroad = arrivalDateAbroad,
        skipPastDates = skipPastDates,
        completeMonth = completeMonth,
        completeResidence = completeResidence)
  }

}
