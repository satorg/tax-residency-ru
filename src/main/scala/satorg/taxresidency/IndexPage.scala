package satorg.taxresidency

import org.scalajs.dom
import org.scalajs.dom.html
import satorg.{dom => sdom}

import scala.scalajs.js.annotation.JSExportTopLevel

object IndexPage {

  private val datesAbroadController = new DatesAbroadController
  private val optionsController = new OptionsController

  private val calculateButton = dom.document.getElementById("calculate-button").asInstanceOf[html.Button]

  @JSExportTopLevel("satorg.taxresidency.index.render")
  def setupUI(): Unit = {

    calculateButton.onclick = calculateButton_onClick
  }

  private def calculateButton_onClick(ev: dom.MouseEvent): Unit = {

    val options = optionsController.options()

    datesAbroadController.retrieveRangeValues().
      fold(
        { dom.window.alert },
        { dateRanges =>

          val resultsUrl = buildResultsURL(UrlParams(dateRanges.toList, options))

          dom.window.open(url = resultsUrl)
        })
  }

  private def buildResultsURL(params: UrlParams) = {

    val url = new sdom.URL("results.html", dom.document.URL)
    UrlParams.write(url.searchParams, params)

    url.href
  }
}
