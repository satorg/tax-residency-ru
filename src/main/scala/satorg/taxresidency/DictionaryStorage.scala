package satorg.taxresidency

import mouse.all._
import org.scalajs.dom.ext.Storage
import satorg.dom.ext.ObjectStorage

import scala.collection.mutable
import scala.scalajs.js

private object DictionaryStorage {

  // TODO: consider replacing with alphabet-based ID generation.
  private def generateNewId(entries: js.Dictionary[_]) = {

    entries.keysIterator.
      map { Integer.parseUnsignedInt }.
      fold(0) { math.max } + 1
  }.toString
}

// TODO: consider caching entries inside.
final class DictionaryStorage[A <: js.Any] private(underlying: ObjectStorage[js.Dictionary[A]])
  extends mutable.AbstractIterable[(String, A)] {

  import DictionaryStorage._

  def this(domExtStorage: Storage, domStorageKey: String) =
    this(new ObjectStorage[js.Dictionary[A]](domExtStorage, domStorageKey, js.Dictionary.empty[A]))

  def add(entry: A): String = {
    // Retrieve a dictionary from the DOM Storage or create a new one.
    val entries = readEntries()

    val newId = generateNewId(entries)

    entries(newId) = entry

    // Store the modified dictionary back to DOM Storage
    writeEntries(entries)

    newId
  }

  def apply(id: String): A = {
    readEntries().apply(id)
  }

  def update(id: String, entry: A): Unit = {
    val entries = readEntries()
    entries(id) = entry
    writeEntries(entries)
  }

  def modify(id: String)(func: A => A): Unit = {
    apply(id) |> { func } |> { update(id, _) }
  }

  def -=(id: String): this.type = {
    val entries = readEntries()
    entries -= id
    writeEntries(entries)
    this
  }

  override def iterator: Iterator[(String, A)] = readEntries().iterator

  def idsIterator: Iterator[String] = readEntries().keysIterator

  def valuesIterator: Iterator[A] = readEntries().valuesIterator

  private def readEntries(): js.Dictionary[A] = underlying.get()

  private def writeEntries(entries: js.Dictionary[A]): Unit = underlying.set(entries)
}
