package satorg.taxresidency

import java.time.LocalDate
import java.time.format.DateTimeParseException

import cats.Semigroupal
import cats.data.{NonEmptyList, Validated, ValidatedNel}
import cats.instances.string._
import cats.syntax.list._
import cats.syntax.option._
import mouse.all._
import org.scalajs.dom
import org.scalajs.dom.html
import satorg.jtime._

import scala.scalajs.js

private object DatesAbroadController {

  private val storageKey = "satorg.taxresidency.datesabroad"

  // TODO: consider avoid serializing via the js.JSON utility.
  private final class EntryData(val departure: String, val arrival: String) extends js.Object {
    def this() = this("", "")
  }

  private implicit final class EntryDataOps(private val self: EntryData) extends AnyVal {

    def copy(departure: String = self.departure, arrival: String = self.arrival) =
      new EntryData(departure = departure, arrival = arrival)
  }

  private type ValidatedType[A] = ValidatedNel[String, A]

  private def parseLocalDate(dateText: String): ValidatedType[LocalDate] = {

    dateText.isEmpty.fold(
      Validated.
        invalidNel("One or more dates are not filled in properly."),
      Validated.
        catchOnly[DateTimeParseException] { LocalDate.parse(dateText) }.
        leftMap { Function.const(s"Failed to parse '$dateText' as a local date") }.
        toValidatedNel
    )
  }

  private def parseEntryData(data: EntryData): ValidatedType[LocalDateRange] = {

    val depDateV = parseLocalDate(data.departure)
    val arrDateV = parseLocalDate(data.arrival)

    Semigroupal.tuple2(depDateV, arrDateV).
      map { Function.tupled(LocalDateRange.build since _ through _) }.
      ensure(NonEmptyList.one(s"Invalid date range: ${ data.departure }..${ data.arrival }")) { _.nonEmpty }
  }
}

final class DatesAbroadController {

  import DatesAbroadController._

  private val storage = new DictionaryStorage[EntryData](dom.ext.LocalStorage, storageKey)

  private val datesAbroadInputGroupTemplate = {
    val elements = dom.document.getElementById("dates-abroad-inputs-template").getElementsByClassName("input-group")
    assert(elements.length == 1)

    elements(0).asInstanceOf[html.Div]
  }

  private val datesAbroadInputsContainer =
    dom.document.getElementById("dates-abroad-inputs-container").asInstanceOf[html.Div]

  // Initialize components.
  {
    for (entryId <- storage.idsIterator) {
      createInput(entryId)
    }

    dom.document.getElementById("add-dates-range-button").asInstanceOf[html.Button] |> { addDatesRangeButton =>

      addDatesRangeButton.onclick = addDatesRangeButton_onClick
    }
  }

  def retrieveRangeValues(): Validated[String, NonEmptyList[LocalDateRange]] = {

    storage.
      valuesIterator.
      toList.toNel.
      toValidNel("No date range defined. Please, enter one or more valid date ranges.").
      andThen {
        _.traverse[ValidatedType, LocalDateRange] { parseEntryData }
      }.
      leftMap {
        _.distinct.toList.mkString("There're one or more errors found:\n- ", "\n- ", "")
      }
  }

  private def addDatesRangeButton_onClick(ev: dom.MouseEvent): Unit = {

    createInput(storage.add(new EntryData))
  }

  private def createInput(entryId: String): Unit = {

    // Clone a new input group from the template.
    val inputGroup = datesAbroadInputGroupTemplate.cloneNode(deep = true).asInstanceOf[html.Div]

    // Retrieve input elements.
    val (departureDateInput, arrivalDateInput) =
      inputGroup.getElementsByTagName("input") |> { inputCollection =>
        assert(inputCollection.length == 2)

        (
          inputCollection.namedItem("departure-date").asInstanceOf[html.Input],
          inputCollection.namedItem("arrival-date").asInstanceOf[html.Input])
      }

    // Set actual input values.
    storage(entryId) |> { entry =>
      departureDateInput.value = entry.departure
      arrivalDateInput.value = entry.arrival

      departureDateInput.max = entry.arrival
      arrivalDateInput.min = entry.departure
    }

    departureDateInput.oninput = { _ =>
      val departure = departureDateInput.value
      storage.modify(entryId) { _.copy(departure = departure) }
      arrivalDateInput.min = departure
    }
    arrivalDateInput.oninput = { _ =>
      val arrival = arrivalDateInput.value
      storage.modify(entryId) { _.copy(arrival = arrival) }
      departureDateInput.max = arrival
    }

    val delDatesRangeButton =
      inputGroup.getElementsByTagName("button") |> { buttonCollection =>
        assert(buttonCollection.length == 1)

        buttonCollection(0).asInstanceOf[html.Button]
      }

    delDatesRangeButton.onclick = { _ =>
      storage -= entryId
      datesAbroadInputsContainer.removeChild(inputGroup)
    }

    datesAbroadInputsContainer.appendChild(inputGroup)
  }
}
