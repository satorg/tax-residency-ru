package satorg.taxresidency

import mouse.all._
import org.scalajs.dom
import org.scalajs.dom.html
import satorg.{dom => sdom}

import scala.scalajs.js.annotation.JSExportTopLevel

object ResultsPage {

  private val params = UrlParams.read(new sdom.URL(dom.document.URL).searchParams)

  private val startupMessageDiv = dom.document.getElementById("startup-message").asInstanceOf[html.Div]
  private val resultsTable = dom.document.getElementById("results-table").asInstanceOf[html.Table]

  @JSExportTopLevel("satorg.taxresidency.results.render")
  def render(): Unit = {

    val resultsTableBody =
      Calculator.run(params.datesAbroad, params.options) |> ResultsRenderer.renderTableBody

    startupMessageDiv |> { startupMessage =>
      startupMessage.parentNode.removeChild(startupMessage)
    }

    resultsTable.appendChild(resultsTableBody)
    resultsTable.removeAttribute("hidden")
  }
}
