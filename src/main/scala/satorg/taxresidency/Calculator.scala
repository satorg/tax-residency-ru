package satorg.taxresidency

import java.time.LocalDate

import satorg.jtime._

object Calculator {
  private final val minDaysHomePermitted = 183

  final case class ResultData(daysHome: Int, daysAbroad: Int, isAbroad: Boolean, isResident: Boolean)

  final case class DateResult(date: LocalDate, result: ResultData)

  def run(datesAbroadRanges: Seq[LocalDateRange], options: Options): Vector[DateResult] = {

    val datesAbroad = calcDatesAbroad(datesAbroadRanges, options)
    val initialDateRange = calcInitialRange(datesAbroad, options)

    val initialDateResults = initialDateRange.iterator.map { calcDateResult(_, datesAbroad) }.toVector

    initialDateResults ++
      initialDateResults.lastOption.iterator.flatMap { finalizeCalc(_, datesAbroad, options) }
  }

  private def calcDateResult(date: LocalDate, datesAbroad: Set[LocalDate]) = {

    val yearRange = LocalDateRange.build after (date minusYears 1) through date

    val daysAbroad = yearRange.count(datesAbroad.contains)
    val daysHome = yearRange.length - daysAbroad

    val isAbroad = datesAbroad.contains(date)
    val isResident = daysHome >= minDaysHomePermitted

    DateResult(
      date = date,
      result = ResultData(
        daysHome = daysHome,
        daysAbroad = daysAbroad,
        isAbroad = isAbroad,
        isResident = isResident))
  }

  private def calcDatesAbroad(datesAbroadRanges: Seq[LocalDateRange], options: Options): Set[LocalDate] = {

    datesAbroadRanges.
      iterator.
      collect {
        case LocalDateRange(departureDate, arrivalDate) =>

          (options.departureDateAbroad, options.arrivalDateAbroad) match {
            case (false, false) =>
              LocalDateRange.build after departureDate before arrivalDate
            case (true, false) if departureDate < arrivalDate =>
              LocalDateRange.build since departureDate before arrivalDate
            case (false, true) if departureDate < arrivalDate =>
              LocalDateRange.build after departureDate through arrivalDate
            case _ =>
              // If both `options.departureDateAbroad` and `options.arrivalDateAbroad` are `true`
              // OR at least one of above is true and both departure and arrival are on the same date.
              LocalDateRange.build since departureDate through arrivalDate
          }
      }.
      flatten.
      toSet
  }

  private def calcInitialRange(datesAbroad: Set[LocalDate], options: Options) = {

    if (datesAbroad.isEmpty) LocalDateRange.empty
    else
      LocalDateRange.build.
        since {
          val minDate = datesAbroad.min

          if (options.skipPastDates)
            implicitly[Ordering[LocalDate]].max(minDate, LocalDate.now())
          else
            minDate
        }.
        through {
          datesAbroad.max
        }
  }

  private def finalizeCalc(lastDateResult: DateResult,
                           datesAbroad: Set[LocalDate],
                           options: Options)
  : Stream[DateResult] = {

    @inline
    def calcNextDate() = {
      val nextDateResult = calcDateResult(lastDateResult.date plusDays 1, datesAbroad)
      nextDateResult #:: finalizeCalc(nextDateResult, datesAbroad, options)
    }

    if (options.completeResidence && !lastDateResult.result.isResident)
      calcNextDate()
    else if (options.completeMonth && !lastDateResult.date.isLastDayOfMonth)
      calcNextDate()
    else Stream.empty
  }
}
