package satorg.taxresidency

import java.time.YearMonth

import mouse.all._
import org.scalajs.dom.html
import satorg.collection._
import satorg.jtime._
import satorg.taxresidency.Calculator.{DateResult, ResultData}
import scalatags.JsDom.all._

object ResultsRenderer {

  def renderTableBody(data: Seq[DateResult]): html.TableSection = {

    tbody {
      data.
        groupAdjacentMap {
          case DateResult(date, _) => date.getYearMonth
        } {
          case DateResult(date, result) => date.getDayOfMonth -> result
        }.
        flatMap { Function.tupled(renderYearMonth) }
    }
  }.render

  private def renderYearMonth(yearMonth: YearMonth, monthData: Seq[(Int, ResultData)]) = {
    assert(monthData.nonEmpty, "month data must not be empty")

    val dayDataLines =
      monthData.
        groupAdjacentBy {
          case (_, resultData) => (resultData.isAbroad, resultData.isResident)
        }.
        iterator.
        map {
          case (_, group) if group.size < 4 =>
            group.map(Some(_))
          case (_, group) =>
            Some(group.head) :: None :: Some(group.last) :: Nil
        }.
        flatMap { group =>

          val firstDayLine =
            group.head.get |> {
              case (dayOfMonth, resultData) => renderGroupDayFirst(dayOfMonth, resultData, group.size)
            }

          val otherDayLines =
            group.tail.map {
              _.fold(tdsGroupDayStub) { Function.tupled(renderGroupDayCommon) }
            }

          firstDayLine +: otherDayLines
        }.
        toVector

    val firstRow =
      tr {
        tdMonth(yearMonth.toString(), dayDataLines.length) :: dayDataLines.head
      }

    val otherRows = for (dayDataLine <- dayDataLines.tail) yield tr { dayDataLine }

    firstRow +: otherRows
  }

  private def renderGroupDayFirst(dayOfMonth: Int, dayData: ResultData, groupSize: Int) = {

    tdDayOfMonth(dayOfMonth) ::
      tdLocation(dayData.isAbroad, groupSize) ::
      renderDaysTotal(dayData) :::
      tdResidence(dayData.isResident, groupSize) ::
      Nil
  }

  private def renderGroupDayCommon(dayOfMonth: Int, dayData: ResultData) = {
    tdDayOfMonth(dayOfMonth) :: renderDaysTotal(dayData)
  }

  private def renderDaysTotal(dayData: ResultData) = {
    td(dayData.daysHome) :: td(dayData.daysAbroad) :: Nil
  }

  private val hellip = raw("&hellip;")

  private val tdStubDayOfMonth = tdDayOfMonth(hellip)
  private val tdStubDaysHome = td(hellip)
  private val tdStubDaysAbroad = td(hellip)
  private val tdsGroupDayStub = tdStubDayOfMonth :: tdStubDaysHome :: tdStubDaysAbroad :: Nil

  private def tdMonth(content: Modifier, rowCount: Int) = th(content, rowspan := rowCount)

  private def tdDayOfMonth(content: Modifier) = td(content)

  private def tdLocation(isAbroad: Boolean, size: Int) = {

    val (innerText, classText) = isAbroad.fold(("Abroad", "table-warning"), ("Home", "table-info"))

    td(innerText, `class` := classText, rowspan := size)
  }

  private def tdResidence(isResident: Boolean, size: Int) = {

    val (innerText, classText) = isResident.fold(("Yes", "table-success"), ("No", "table-danger font-weight-bold"))

    td(innerText, `class` := classText, rowspan := size)
  }
}
