package satorg.taxresidency

import java.time.LocalDate

import satorg.jtime._
import satorg.{dom => sdom}

final case class UrlParams(datesAbroad: Seq[LocalDateRange], options: Options)

object UrlParams {

  private final val DateRange = "dr"
  private final val DepartureDateAbroad = "dd"
  private final val ArrivalDateAbroad = "ad"
  private final val SkipPastDates = "sp"
  private final val CompleteResidence = "cr"
  private final val CompleteMonth = "cm"

  private val dateRangeRe = """(\d{4}\-\d{2}-\d{2})_(\d{4}\-\d{2}-\d{2})""".r

  private def formatDateRange(dateRange: LocalDateRange): String = s"${ dateRange.head }_${ dateRange.last }"

  private def parseDateRange(param: String): LocalDateRange = {

    param match {
      case dateRangeRe(startDate, endDate) =>
        LocalDate.parse(startDate) to LocalDate.parse(endDate)
    }
  }

  def read(searchParams: sdom.URLSearchParams): UrlParams = {

    val datesAbroad = searchParams.getAll(DateRange).iterator.map { parseDateRange }.toSeq

    val options =
      new Options(
        departureDateAbroad = searchParams.has(DepartureDateAbroad),
        arrivalDateAbroad = searchParams.has(ArrivalDateAbroad),
        skipPastDates = searchParams.has(SkipPastDates),
        completeResidence = searchParams.has(CompleteResidence),
        completeMonth = searchParams.has(CompleteMonth))

    UrlParams(datesAbroad, options)
  }

  def write(searchParams: sdom.URLSearchParams, params: UrlParams): Unit = {

    for (dateRange <- params.datesAbroad) {
      searchParams.append(DateRange, formatDateRange(dateRange))
    }

    @inline
    def setOption(name: String, defined: Boolean): Unit = { if (defined) searchParams.set(name, "") }

    setOption(DepartureDateAbroad, params.options.departureDateAbroad)
    setOption(ArrivalDateAbroad, params.options.arrivalDateAbroad)
    setOption(SkipPastDates, params.options.skipPastDates)
    setOption(CompleteResidence, params.options.completeResidence)
    setOption(CompleteMonth, params.options.completeMonth)
  }
}
