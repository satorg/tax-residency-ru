package satorg.taxresidency

import mouse.all._
import org.scalajs.dom
import org.scalajs.dom.html
import satorg.dom.ext.ObjectStorage

object OptionsController {

  private val storageKey = "satorg.taxresidency.options"

  private val optionsStorage = new ObjectStorage[Options](dom.ext.LocalStorage, storageKey, Options.Default)
}

final class OptionsController {

  import OptionsController._

  private val departureDateAbroadCheckbox =
    dom.document.getElementById("departure-date-abroad-checkbox").asInstanceOf[html.Input]

  private val arrivalDateAbroadCheckbox =
    dom.document.getElementById("arrival-date-abroad-checkbox").asInstanceOf[html.Input]

  private val skipPastDatesCheckbox =
    dom.document.getElementById("skip-past-dates-checkbox").asInstanceOf[html.Input]

  private val completeResidenceCheckbox =
    dom.document.getElementById("complete-residence-checkbox").asInstanceOf[html.Input]

  private val completeMonthCheckbox =
    dom.document.getElementById("complete-month-checkbox").asInstanceOf[html.Input]

  // Initialize components.
  {
    val options = optionsStorage.get()

    departureDateAbroadCheckbox.checked = options.departureDateAbroad
    arrivalDateAbroadCheckbox.checked = options.arrivalDateAbroad
    skipPastDatesCheckbox.checked = options.skipPastDates
    completeResidenceCheckbox.checked = options.completeResidence
    completeMonthCheckbox.checked = options.completeMonth

    onChangeOption(departureDateAbroadCheckbox) { value => _.copy(departureDateAbroad = value) }
    onChangeOption(arrivalDateAbroadCheckbox) { value => _.copy(arrivalDateAbroad = value) }
    onChangeOption(skipPastDatesCheckbox) { value => _.copy(skipPastDates = value) }
    onChangeOption(completeResidenceCheckbox) { value => _.copy(completeResidence = value) }
    onChangeOption(completeMonthCheckbox) { value => _.copy(completeMonth = value) }
  }

  def options(): Options = optionsStorage.get()

  private def onChangeOption(checkbox: html.Input)(handler: Boolean => Options => Options): Unit = {

    checkbox.oninput = { _ =>
      optionsStorage.get() |> handler(checkbox.checked) |> optionsStorage.set
    }
  }
}
