package satorg

import java.time.{LocalDate, YearMonth}

package object jtime {

  implicit final class LocalDateOrdered(private val self: LocalDate) extends AnyVal with Ordered[LocalDate] {
    override def compare(that: LocalDate): Int = self compareTo that
  }

  implicit final class LocalDateOps(private val self: LocalDate) extends AnyVal {

    def to(that: LocalDate): LocalDateRange = LocalDateRange.build since self through that

    def until(that: LocalDate): LocalDateRange = LocalDateRange.build since self before that

    @inline
    def getYearMonth: YearMonth = YearMonth.of(self.getYear, self.getMonthValue)

    @inline
    def isLastDayOfMonth: Boolean = self.getDayOfMonth == self.lengthOfMonth()
  }

}
