package satorg.jtime

import java.time.LocalDate
import java.time.temporal.ChronoUnit.DAYS

import mouse.all._

import scala.collection.{AbstractSeq, immutable}

abstract sealed class LocalDateRange extends AbstractSeq[LocalDate] with immutable.IndexedSeq[LocalDate] {

  final override def lengthCompare(len: Int): Int = length - len
}

object LocalDateRange {

  private object Empty extends LocalDateRange {

    override def length: Int = 0

    override val isEmpty: Boolean = true

    override def apply(idx: Int): LocalDate = throw new IndexOutOfBoundsException(s"empty date range")

    override def toString(): String = "LocalDateRange.Empty"
  }

  private final class Inclusive(override val head: LocalDate, override val last: LocalDate) extends LocalDateRange {
    require(head <= last, s"${ getClass.getSimpleName }: `head` ($head) must not be greater than `last` ($last)")

    override lazy val length: Int = DAYS.between(head, last) |> Math.toIntExact |> Math.incrementExact

    override lazy val isEmpty: Boolean = false

    override def apply(idx: Int): LocalDate = {
      if (idx < 0 || idx >= length)
        throw new IndexOutOfBoundsException(s"$idx is out of bounds (0..${ length - 1 })")

      head plusDays idx
    }

    override def min[B >: LocalDate : Ordering]: LocalDate = head

    override def max[B >: LocalDate : Ordering]: LocalDate = last

    override def contains[B >: LocalDate](elem: B): Boolean =
      PartialFunction.cond(elem) { case that: LocalDate => (head <= that) && (that <= last) }

    override def toString(): String = s"LocalDateRange($head..$last)"
  }

  def apply(head: LocalDate, last: LocalDate): LocalDateRange = {

    if (head <= last) new Inclusive(head, last) else Empty
  }

  def build: BuilderStart = new BuilderStart

  final class BuilderStart private[LocalDateRange] {

    def since(head: LocalDate): BuilderEnd = new BuilderEnd(head)

    def after(head: LocalDate): BuilderEnd = new BuilderEnd(head plusDays 1)

  }

  final class BuilderEnd private[LocalDateRange](head: LocalDate) {

    def before(last: LocalDate): LocalDateRange = apply(head, last minusDays 1)

    def through(last: LocalDate): LocalDateRange = apply(head, last)
  }

  def unapply(range: LocalDateRange): Option[(LocalDate, LocalDate)] = {

    range.nonEmpty option { (range.head, range.last) }
  }

  def empty: LocalDateRange = Empty
}
