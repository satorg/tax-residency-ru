package satorg.collection

import org.scalatest.{FreeSpec, Matchers}

class SeqOpsTest extends FreeSpec with Matchers {

  def flattenRes[A, K](res: Seq[(K, Seq[A])]): Seq[A] = res.flatMap(_._2)

  "groupAdjacentBy" - {
    "Seq" - {
      "seq is empty" in {
        Seq.empty.groupAdjacentBy(identity[Int]) shouldBe empty
      }
      "seq has a single element" in {
        Seq(123).groupAdjacentBy(identity) shouldBe Seq(123 -> Seq(123))
      }
      "seq has all different elements" in {
        val src = Seq(1, 2, 3)
        val res = Seq(1 -> Seq(1), 2 -> Seq(2), 3 -> Seq(3))

        src.groupAdjacentBy(identity) shouldBe res
        flattenRes(res) shouldBe src
      }
      "there're some equal adjacent element in a seq" in {
        val src = Seq(1, 2, 2, 3, 3, 3, 2, 2, 1)
        val res = Seq(1 -> Seq(1), 2 -> Seq(2, 2), 3 -> Seq(3, 3, 3), 2 -> Seq(2, 2), 1 -> Seq(1))

        src.groupAdjacentBy(identity) shouldBe res
        flattenRes(res) shouldBe src
      }
      "applying a non-trivial key function" in {
        val src = Seq(11, 12, 22, 21, 13, 14, 24, 34, 33, 23, 13)

        val res1 =
          Seq(
            1 -> Seq(11, 12),
            2 -> Seq(22, 21),
            1 -> Seq(13, 14),
            2 -> Seq(24),
            3 -> Seq(34, 33),
            2 -> Seq(23),
            1 -> Seq(13))

        src.groupAdjacentBy(_ / 10) shouldBe res1
        flattenRes(res1) shouldBe src

        val res2 =
          Seq(
            1 -> Seq(11),
            2 -> Seq(12, 22),
            1 -> Seq(21),
            3 -> Seq(13),
            4 -> Seq(14, 24, 34),
            3 -> Seq(33, 23, 13))

        src.groupAdjacentBy(_ % 10) shouldBe res2
        flattenRes(res2) shouldBe src
      }
    }
  }
}
