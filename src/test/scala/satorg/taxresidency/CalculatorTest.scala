package satorg.taxresidency

import java.time.LocalDate

import org.scalatest.Matchers._
import org.scalatest.prop.TableDrivenPropertyChecks._
import org.scalatest.{Assertion, FreeSpec}
import satorg.jtime.LocalDateRange

class CalculatorTest extends FreeSpec {

  import CalculatorTest._

  def forAllOptions[R](body: Options => Assertion): Assertion =
    forAll(possibleOptions) {
      (
        departureDateAbroad: Boolean,
        arrivalDateAbroad: Boolean,
        skipPastDates: Boolean,
        completeResidence: Boolean,
        completeMonth: Boolean) =>

        val options =
          new Options(
            departureDateAbroad = departureDateAbroad,
            arrivalDateAbroad = arrivalDateAbroad,
            skipPastDates = skipPastDates,
            completeResidence = completeResidence,
            completeMonth = completeMonth)

        body(options)
    }

  "should handle an empty range list" in {
    forAllOptions { options =>

      Calculator.run(Nil, options) shouldBe empty
    }
  }
  "should handle a single empty date range" in {
    forAllOptions { options =>
      Calculator.run(LocalDateRange.empty :: Nil, options) shouldBe empty
    }
  }
  "should handle a range with the same departure and arrival date" in {
    val range = LocalDateRange.build since LocalDate.now() through LocalDate.now()
    forAllOptions { options =>
      whenever(!options.departureDateAbroad && !options.arrivalDateAbroad) {
        Calculator.run(range :: Nil, options) shouldBe empty
      }
    }
  }
}

//noinspection VariablePatternShadow
object CalculatorTest {

  private def intToBitList(value: Int, len: Int): List[Boolean] = {

    if (len == 0) Nil
    else {
      val nextLen = len - 1
      val bitOn = (value & (1 << nextLen)) != 0
      bitOn :: intToBitList(value, nextLen)
    }
  }

  private def genAllTuples(): Seq[(Boolean, Boolean, Boolean, Boolean, Boolean)] = {

    (0 to 31).map { intToBitList(_, 5) }.collect {
      case a :: b :: c :: d :: e :: Nil => (a, b, c, d, e)
    }
  }


  lazy val possibleOptions = Table(
    ("departureDateAbroad", "arrivalDateAbroad", "skipPastDates", "completeResidence", "completeMonth"),
    genAllTuples(): _*
  )
}
