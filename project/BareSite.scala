import java.nio.file.{Files, StandardCopyOption}

import org.scalajs.sbtplugin.ScalaJSPlugin.AutoImport.{fastOptJS, fullOptJS, scalaJSSourceMap}
import sbt.Keys._
import sbt.{Def, _}

object BareSite {
  lazy val siteDirectoryName = settingKey[String]("name of the directory where to copy site files")
  lazy val siteOutputDirectoryName = settingKey[String]("name of the directory where to copy compiled files")
  lazy val siteOutputFileName = settingKey[String]("overrides name of compiled files")
  lazy val siteDev = taskKey[Unit]("prepare static web site for dev")
  lazy val siteProd = taskKey[Unit]("prepare static web site for prod")

  private def makeTask(sourceTask: TaskKey[Attributed[File]]) = Def.task {
    val log = streams.value.log
    log.debug(s"${ getClass.getSimpleName } task is executing on ${ sourceTask.key.label }")

    val websiteDir = (Compile / target).value / siteDirectoryName.value
    val websiteJsDir = websiteDir / siteOutputDirectoryName.value

    Files.createDirectories(websiteJsDir.toPath)

    val resourceFiles = (Compile / resources).value.filter(_.isFile)
    val compiledFiles =
      (Compile / sourceTask).map { attributed => attributed.data :: attributed.get(scalaJSSourceMap).toList }.value

    // Copy resource files
    log.info(s"Copy ${ resourceFiles.size + compiledFiles.size } file(s) to $websiteDir")
    for (file <- resourceFiles) {
      val fileTarget = websiteDir / file.getName
      log.debug(s"""copy "$file" to "$fileTarget"""")

      Files.copy(file.toPath, fileTarget.toPath, StandardCopyOption.REPLACE_EXISTING)
    }

    // Copy compiled files
    val configuredOutputName = siteOutputFileName.value
    lazy val defaultArtifactName = (Compile / sourceTask / artifactPath).value.getName

    for (file <- compiledFiles) {

      val fileName = file.getName
      val resultFileName =
        if (configuredOutputName.isEmpty || !(fileName startsWith defaultArtifactName)) fileName
        else {
          configuredOutputName + (fileName drop defaultArtifactName.length)
        }

      val fileTarget = websiteJsDir / resultFileName

      log.debug(s"""copy "$file" to "$fileTarget"""")
      Files.copy(file.toPath, fileTarget.toPath, StandardCopyOption.REPLACE_EXISTING)
    }
  }

  def siteSettings(): Def.SettingsDefinition = Seq(
    siteDirectoryName := "site",
    siteOutputDirectoryName := "js",
    siteOutputFileName := "",
    siteDev := BareSite.makeTask(Compile / fastOptJS).value,
    siteProd := BareSite.makeTask(Compile / fullOptJS).value
  )
}
