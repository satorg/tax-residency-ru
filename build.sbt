ThisBuild / organization := "satorg"
ThisBuild / version := "0.1"
ThisBuild / scalaVersion := "2.12.7"
ThisBuild / scalacOptions ++= Seq(
  "-deprecation",
  "-feature",
  "-unchecked",
  "-Xfatal-warnings",
  "-Ypartial-unification"
)

import BareSite._

lazy val root = (project in file(".")).
  enablePlugins(ScalaJSPlugin).
  settings(
    name := "tax-residency-ru",
    libraryDependencies ++= Seq(
      "org.typelevel" %%% "cats-core" % "1.4.0",
      "org.typelevel" %%% "mouse" % "0.19",
      "org.scala-js" %%% "scalajs-dom" % "0.9.6",
      "org.scala-js" %%% "scalajs-java-time" % "0.2.5",
      "com.lihaoyi" %%% "scalatags" % "0.6.7",
      "org.scalatest" %%% "scalatest" % "3.0.5" % Test,
      "org.scalacheck" %%% "scalacheck" % "1.14.0" % Test
    ),
    scalacOptions += "-P:scalajs:sjsDefinedByDefault",
    packageJSDependencies / skip := true,
    packageMinifiedJSDependencies / skip := true,
    siteSettings(),
    siteOutputFileName := "main.js"
  )
